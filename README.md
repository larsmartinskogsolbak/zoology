A simple console application to manage a group of animals. 

It includes an Animal Class with properties for creating Animal objects of different species. 

The Animal objects are stored in a List. Information about the animals are displayed in a console by accessing them from the List. 