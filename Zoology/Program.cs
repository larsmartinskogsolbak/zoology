﻿using System;
using System.Collections.Generic;

namespace Zoology
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal cat = new Animal("Cat", "Meowy"); 

            List<Animal> animals = new List<Animal>
            {
                new Animal("Dog", "Woofie", 5),
                new Animal("Turtle", "Bert", 10)
            };

            animals.Add(cat);
            cat.Sleep(8);

            Console.WriteLine("Animals:");
            foreach (Animal animal in animals)
            {
                Console.WriteLine("Name: " + animal.Name);
                Console.WriteLine("Species: " + animal.Species);
                
            }
        }
        }
    }
