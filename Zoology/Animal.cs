﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    class Animal
    { 
        private string species; 
        private string name;

        public string Species { get => species; set => species = value; }
        public string Name { get => name; set => name = value; }

        public Animal(string species, string name)
        {
            Name = name;
            Species = species;
        }
        public Animal(string species, string name, int sleep)
        {
            Name = name;
            Species = species;
            Sleep(sleep);
        }

            public void Sleep(int hours)
        {
            Console.WriteLine(this.name + " slept for " + hours);
        }
    }
}
